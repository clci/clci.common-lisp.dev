;;; This file is part of clci-site. See LICENSE and README.md for more
;;; information.

(in-package #:clci-site)

(defsection @index (:title "CL Continuous Integration")
  "The purpose of this project is to encourage widespread use of continuous
integration for Common Lisp projects. To that end, we will gather links to
solutions for various CI platforms, as well as implement new solutions."
  (@ci-platforms section))

(defsection @ci-platforms (:title "CI Platforms")
  "In this section, we give an overview of CI platforms and solutions built on
those platforms, tailored for Common Lisp projects.

We know this section is likely woefully incomplete. If you know of others,
please open a Merge Request at
<https://gitlab.common-lisp.net/clci/clci.common-lisp.dev/>"
  (@gitlab-ci section)
  (@github-actions section)
  (@travis-ci section))

(defun generate-docs ()
  (let ((40ants-doc/builder/printer:*document-uppercase-is-code* nil))
    (40ants-doc/builder:update-asdf-system-docs (list @index)
                                                :clci-site)
    (40ants-doc/builder:update-asdf-system-docs (list @clci-gitlab-ci)
                                                :clci-site)
    (40ants-doc/builder:update-asdf-system-docs (list @clci-gitlab-ci/2.0.0)
                                                :clci-site)))
