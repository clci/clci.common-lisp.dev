;;; This file is part of clci-site. See LICENSE and README.md for more
;;; information.

(in-package #:clci-site)

(defsection @travis-ci (:title "Travis CI")
  "Most CI Platforms these days owe their success to Travis CI. Travis was one
of the first (if not the first) commercial CI platform that opened its doors to
projects for free and started to get devs to really use CI. Travis CI
documentation can be found at <https://docs.travis-ci.com/>.

Unfortunately, Travis has fallen far from its perch after an acquisition by
Idera. They still offer free credits to Open Source projects. But in reality,
these credits are very difficult to obtain. New projects should likely not use
Travis CI unless they are willing to pay. Existing projects that use Travis CI
should consider moving to another solution or pay.

Since Travis CI is one of the oldest CI platforms out there, it additionally
comes with a lot of baggage. While modern platforms make it trivial to have CI
pipelines that do more than just run tests, it is not so obvious how to do that
on Travis. The easiest thing to do with Travis is write a script that depends
on environment variables, and then Travis will run it N times for you, with the
values of the variables chose from a matrix."

  (@travis-ci-helpers section))

(defsection @travis-ci-helpers (:title "Helpers")
  (@travis-ci-helpers-cl-travis section)
  (@travis-ci-helpers-roswell section))

(defsection @travis-ci-helpers-cl-travis (:title "CL Travis")
  "The [CL Travis Project](https://github.com/lispci/cl-travis) is probably the
best resource for using Travis CI with CL projects.

The biggest concern with CL Travis is likely that it use
[CIM](https://github.com/sionescu/CIM) (a deprecated project) under the
hood. Additionally, it has hard coded versions of CL implementations that it
uses. These versions do seem to be kept up to date, however.")

(defsection @travis-ci-helpers-roswell (:title "Roswell")
  "Roswell's wiki has some information on using Roswell on Travis CI. See
<https://github.com/roswell/roswell/wiki/Travis-CI>.

Roswell must build several implementations locally in order to use them. So if
you use Roswell, be sure to look at their section on caching to ensure that you
don't continuously waste time building the same implementation over and over again.")
