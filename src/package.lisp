;;; This file is part of clci-site. See LICENSE and README.md for more
;;; information.

(uiop:define-package #:clci-site
  (:use #:cl
        #:40ants-doc))
