;;; This file is part of clci-site. See LICENSE and README.md for more
;;; information.

(in-package #:clci-site)

(defsection @github-actions (:title "GitHub Actions")
  "GitHub Actions is a relative newcomer on the scene. It is tightly integrated
into GitHub. You should probably consider using it if your project is hosted on
GitHub. Its documentation is located at <https://docs.github.com/en/actions>.

While GitHub Actions has a large allocation of free minutes, you should
consider the possibility that may change in the future. Like many CI platforms,
GitHub Actions allows you to provide your own runner, thereby getting around
the free minutes cap. However, GitHub has questionably decided to allow forks
of repositories to run jobs on the upstream's self-hosted runners by
default. This presents some serious security concerns if you use self-hosted
runners for public projects and GitHub explicitly recommends against it:
<https://docs.github.com/en/actions/hosting-your-own-runners/about-self-hosted-runners#self-hosted-runner-security>

Because it is a relative newcomer, GitHub Actions has the benefit of learning
from the mistakes of others. It is very clearly designed from the ground up to
be a very modular system with few assumptions.

The downside to this is that it can feel very \"enterprise\"y and can be very
difficult to get started. Everything is very explicit. For example: you must
include an explicit step to check out your repository in a workflow.

An upside is that they have made it very easy to share workflow actions. That
is, so long as you don't mind writing those actions in JavaScript or piecing
them together from other actions."

  (@github-actions-helpers section))

(defsection @github-actions-helpers (:title "Helpers")
  (@github-actions-helpers-40ants section)
  (@github-actions-helpers-3b section))

(defsection @github-actions-helpers-40ants (:title "40Ants")
  "As part of the 40 Ants organization,
[svetlyak40wt](https://github.com/svetlyak40wt) has developed
[40ants/ci](https://github.com/40ants/ci). This project allows you to write
some CL code describing GitHub Actions Workflows and then they are translated
into the YAML files needed for GitHub Actions. By its own admission, it is very
opinionated, and that may be a turn off for some folks. However, it appears to
be of high quality and is worth checking out.

In addition to the workflow generator, the 40 Ants-verse has developed several
individual Actions that can be used in any workflow. See:

- <https://github.com/40ants/setup-lisp>
- <https://github.com/40ants/run-tests>
- <https://github.com/40ants/build-docs>")

(defsection @github-actions-helpers-3b (:title "3B")
  "[3b](https://github.com/3b) has put together several blog posts on using CL
with GitHub Actions.

Unfortunately, it's not something you can just drop into your repo and have it
work, but he does show how you can build your own workflows using several tools
that are not the 40Ants tools.

- <http://3bb.cc/blog/2020/09/11/github-ci/>
- <http://3bb.cc/blog/2020/09/13/github-ci2/>")
