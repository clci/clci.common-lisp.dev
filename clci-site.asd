;;; -*- mode: lisp -*-

(asdf:defsystem #:clci-site
  :version "0.0.1"
  :author "Eric Timmons <eric@timmons.dev>"
  :description "Website for clci project."
  :license "MIT"
  :depends-on ("40ants-doc-full" "40ants-doc")
  :pathname "src/"
  :components ((:file "package")
               (:file "clci-gitlab-ci" :depends-on ("package"))
               (:file "gitlab-ci" :depends-on ("clci-gitlab-ci"))
               (:file "github" :depends-on ("package"))
               (:file "travis-ci" :depends-on ("package"))
               (:file "clci-site" :depends-on ("package"))))
