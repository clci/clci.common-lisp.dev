;;; This file is part of clci-site. See LICENSE and README.md for more
;;; information.

(in-package #:clci-site)

(defparameter *clci-gitlab-ci-versions* ())

(defmacro define-clci-gitlab-ci-version (version)
  (let ((name (intern (concatenate 'string "@CLCI-GITLAB-CI/" version))))
    `(progn
       (defsection ,name
           (:title ,(concatenate 'string "CLCI Gitlab CI v" version))
         ,(uiop:read-file-string (asdf:system-relative-pathname :clci-site "gitlab-ci/2.0.0/manual.md")))
       (pushnew ,version *clci-gitlab-ci-versions* :test 'equal))))

(define-clci-gitlab-ci-version "2.0.0")
